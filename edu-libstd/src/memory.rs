//! Memory management functionality for the application.

use core::alloc::GlobalAlloc;
use core::alloc::Layout;


pub fn alloc(layout: Layout) -> *mut u8 {
	assert_ne!(layout.size(), 0);
	unsafe { edu_kernel::mm::ALLOCATOR.alloc(layout) }
}

#[allow(clippy::missing_safety_doc)]
pub unsafe fn dealloc(ptr: *mut u8, layout: Layout) {
	edu_kernel::mm::ALLOCATOR.dealloc(ptr, layout)
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_edu_libstd_alloc() {
		let layout = Layout::new::<[u64; 128]>();
		let newmem = alloc(layout);
		unsafe { dealloc(newmem, layout) };
	}
}
