//! Abstractions for Stacks

use crate::consts::STACK_SIZE;

/// A stack of [`STACK_SIZE`], which grows downwards.
#[derive(Copy, Clone)]
#[repr(align(0x200000))]
#[repr(C)]
pub struct Stack {
	buffer: [u8; STACK_SIZE],
}

impl Stack {
	pub const fn new() -> Stack {
		Stack {
			buffer: [0; STACK_SIZE],
		}
	}

	pub fn top(&self) -> usize {
		(&(self.buffer[STACK_SIZE - 16]) as *const _) as usize
	}

	pub fn bottom(&self) -> usize {
		(&(self.buffer[0]) as *const _) as usize
	}
}

/// A statically allocated boot stack, which we can safely switch to directly
/// after boot.
pub static mut BOOT_STACK: Stack = Stack::new();
